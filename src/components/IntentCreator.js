import React, {useState} from 'react';
import IntentFeatures from './IntentFeatures';
import {Dropdown, Button, Icon, Menu, message, Row, Input, Col} from 'antd';

const ButtonGroup = Button.Group;

const TRIGGER_TYPE = {
  ACTION: 'Action based (button on a UI)',
  TIME: "Time based (e.g @5 o'clock each day)",
  GEO: 'Geolocation based (e.g. when the user leaves his home)',
};

const IntentCreator = ({services, onSubmitIntent}) => {
  const [name, setName] = useState ();
  const [trigger, setTrigger] = useState ();

  const [serviceName, setServiceName] = useState ();
  const [featureList, setFeatureList] = useState ({});

  const serviceDefinitions =
    services.filter (({id}) => id === serviceName) &&
    services.filter (({id}) => id === serviceName)[0] &&
    services.filter (({id}) => id === serviceName)[0].definitions &&
    services.filter (({id}) => id === serviceName)[0].definitions
      .settable &&
    Object.keys (
      services.filter (({id}) => id === serviceName)[0].definitions.settable
    );

  const selectServiceDefinition = name =>
    serviceDefinitions &&
    services.filter (({id}) => id === serviceName)[0].definitions.settable[
      name
    ];

  const addToFeatureList = name =>
    setFeatureList ({...featureList, [name]: {value: null}});

  const addValueToFeatureList = (name, value) =>
    setFeatureList ({...featureList, [name]: {value}});

  const submitFeaturesList = () => {
    if (Object.keys (featureList).length < 1) {
      message.warning ('Please add features to your intent');
      return;
    }

    const incompleteFeatures = Object.entries (featureList)
      .filter (([, {value}]) => value === null)
      .map (([name]) => {
        message.warning (`Please fill in the wished value for ${name}`);
        return name;
      });
    if (incompleteFeatures.length === 0) {
      onSubmitIntent ({name, trigger, featureList, serviceName});
      clearIntent()
    }
  };

  const clearIntent = () => {
    setFeatureList ({});
    setName();
    setTrigger();
    setServiceName();
  }

  const submitIntent = () => {
    if (!name || !trigger) {
      message.warning (`Please fill in all fields before submitting`);
    } else {
      submitFeaturesList ();
    }
  };

  const menu = (
    <Menu onClick={({key}) => setTrigger (key)}>
      {Object.entries (TRIGGER_TYPE).map (([key, value]) => (
        <Menu.Item key={key}>{value}</Menu.Item>
      ))}
    </Menu>
  );

  return (
    <Row>
      <h2>Create an automatic process</h2>
      <Row>
        <Col span={8}>
          <Input
            placeholder="Name"
            onChange={({target: {value}}) => setName (value)}
            value={name}
          />
        </Col>
        <Col span={16}>
          <Dropdown overlay={menu}>
            <Button style={{marginLeft: 8}}>
              {trigger ? TRIGGER_TYPE[trigger] : 'Type of Trigger'}
              {' '}
              <Icon type="down" />
            </Button>
          </Dropdown>
        </Col>
      </Row>
      <IntentFeatures
        services={services}
        onSubmitIntent={submitIntent}
        setServiceName={setServiceName}
        serviceName={serviceName}
        serviceDefinitions={serviceDefinitions}
        addToFeatureList={addToFeatureList}
        featureList={featureList}
        addValueToFeatureList={addValueToFeatureList}
        selectServiceDefinition={selectServiceDefinition}
      />
      <Row>
        <ButtonGroup>
          <Button onClick={clearIntent}>Cancel</Button>
          <Button onClick={submitIntent} type="primary">Submit</Button>
        </ButtonGroup>
      </Row>
    </Row>
  );
};

export default IntentCreator;
