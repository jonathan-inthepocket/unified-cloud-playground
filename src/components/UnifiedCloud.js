import React from 'react';
import ReactJson from 'react-json-view';

const defaults = {
  displayDataTypes: false,
  enableClipboard: false,
  enableEdit: false,
  enableAdd: false,
  enableDelete: false,
  displayObjectSize: false,
};
const UnifiedCloud = ({devices, services, intents}) => {
  return (
    <div>
      <div>
        <h1>Intents</h1>
        <div className="devices">
          {intents.map (intent => {
            return (
              <div>
                <h2>{intent.name}</h2>
                <ReactJson {...defaults} src={intent} />
              </div>
            );
          })}
        </div>
      </div>
      <div>
        <h1>Devices</h1>
        <div className="devices">
          {devices.map (device => {
            return (
              <div>
                <h2>{device.name}</h2>
                <ReactJson {...defaults} src={device} />
              </div>
            );
          })}
        </div>
      </div>
      <div>
        <h1>Services</h1>
        <div className="devices">
          {services.map (service => (
            <div>
              <h2>{service.name}</h2><ReactJson {...defaults} src={service} />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default UnifiedCloud;
