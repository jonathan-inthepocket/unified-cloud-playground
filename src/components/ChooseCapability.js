import React from 'react';
import {
  Dropdown,
  Button,
  Icon,
  Menu,
  InputNumber,
  message,
  Row,
  Input,
  Col,
} from 'antd';

const ChooseCapability = ({name, value, onValueChange, capability}) => {
  const selectIntegerValues = integerCapability => {
    if (
      !integerCapability ||
      !integerCapability.validation ||
      !integerCapability.validation.min ||
      !integerCapability.validation.max
    ) {
      return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    }
    const values = [];
    for (
      let index = integerCapability.validation.min.value;
      index < integerCapability.validation.max.value + 1;
      index++
    ) {
      values.push (index);
    }

    return values;
  };

  const selectIntegerMinMax = integerCapability => {
    if (
      !integerCapability ||
      !integerCapability.validation ||
      !integerCapability.validation.min ||
      !integerCapability.validation.max
    ) {
      return {min: null, max: null};
    }
    return {
      min: integerCapability.validation.min.value,
      max: integerCapability.validation.max.value,
      decimals: integerCapability.validation.decimals,
    };
  };

  const selectPredefinedStringValues = stringCapability => {
    if (
      !stringCapability ||
      !stringCapability.validation ||
      !stringCapability.validation.enum ||
      !stringCapability.validation.enum.value
    ) {
      return null;
    }
    return stringCapability.validation.enum.value;
  };

  switch (capability.type) {
    case 'integer':
      return (
        <Dropdown
          overlay={
            <Menu onClick={({key}) => onValueChange (key)}>
              {selectIntegerValues (capability).map (integer => (
                <Menu.Item key={integer}>{integer}</Menu.Item>
              ))}
            </Menu>
          }
        >
          <Button style={{marginLeft: 8}}>
            {value || 'Choose your value'} <Icon type="down" />
          </Button>
        </Dropdown>
      );
    case 'float':
      const {min, max, decimals} = selectIntegerMinMax (capability);
      console.log (
        '---decimals',
        min,
        max,
        decimals,
        1 / Math.pow (10, decimals),
        '\n'
      );
      return (
        <InputNumber
          min={min}
          max={max}
          step={decimals ? 1 / Math.pow (10, decimals) : 0}
          onChange={onValueChange}
        />
      );
    case 'string':
      const predefinedStringValues = selectPredefinedStringValues (capability);
      return predefinedStringValues
        ? <Dropdown
            overlay={
              <Menu onClick={({key}) => onValueChange (key)}>
                {predefinedStringValues.map (stringValue => (
                  <Menu.Item key={stringValue}>{stringValue}</Menu.Item>
                ))}
              </Menu>
            }
          >
            <Button style={{marginLeft: 8}}>
              {value || 'Choose your value'} <Icon type="down" />
            </Button>
          </Dropdown>
        : <Input placeholder={name} />;
    default:
      return 'TYPE NOT YET IMPLEMENTED';
  }
};

export default ChooseCapability;
