import React from 'react';
import Intent from './Intent';

const Intents = ({intents, onApplyIntent}) => {
  return (
    !intents ||
    (intents.length !== 0 &&
      intents.map (intent => {
        return (
          <Intent
            intent={intent}
            onApplyIntent={() => onApplyIntent (intent.name)}
          />
        );
      }))
  );
};

export default Intents;
