import React from 'react';
import { Row, Button } from 'antd';

const Intent = ({intent, onApplyIntent}) => {
  return <Row>
        <Button onClick={onApplyIntent}>{intent.name}</Button>
      </Row>;
};

export default Intent;
