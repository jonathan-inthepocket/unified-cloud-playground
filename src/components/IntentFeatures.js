import React from 'react';
import {Dropdown, Button, Icon, Menu, message, Row, Input, Col} from 'antd';
import ChooseCapability from './ChooseCapability';

const IntentFeatures = ({
  services,
  setServiceName,
  serviceName,
  serviceDefinitions,
  addToFeatureList,
  featureList,
  addValueToFeatureList,
  selectServiceDefinition,
}) => {
  return (
    <Row>
      <h3>Define your intent</h3>
      <Row>
        <Col span={12}>
          <Dropdown
            overlay={
              <Menu onClick={({key}) => setServiceName(key)}>
                {services.map(service => (
                  <Menu.Item key={service.id}>{service.name}</Menu.Item>
                ))}
              </Menu>
            }
          >
            <Button style={{marginLeft: 8}}>
              {(serviceName &&
                services.filter(service => service.id === serviceName)[0]
                  .name) ||
                'Choose a service'}{' '}
              <Icon type="down" />
            </Button>
          </Dropdown>
        </Col>
        <Col span={12}>
          {serviceName && (
            <Dropdown
              overlay={
                <Menu onClick={({key}) => addToFeatureList(key)}>
                  {serviceDefinitions &&
                    serviceDefinitions.map(capabilities => (
                      <Menu.Item key={capabilities}>{capabilities}</Menu.Item>
                    ))}
                </Menu>
              }
            >
              <Button style={{marginLeft: 8}}>
                Add a features you want to manipulate
                <Icon type="down" />
              </Button>
            </Dropdown>
          )}
        </Col>
      </Row>
      {featureList && Object.keys(featureList).length > 0 && (
        <Row>
          <h3>Features included in the intent</h3>
          {Object.entries(featureList).map(([name, target]) => (
            <Row>
              <Col span={4}>{name}</Col>
              <Col span={4}>
                <ChooseCapability
                  name={name}
                  value={target.value}
                  onValueChange={value => addValueToFeatureList(name, value)}
                  capability={selectServiceDefinition(name)}
                />
              </Col>
            </Row>
          ))}
        </Row>
      )}
    </Row>
  );
};

export default IntentFeatures;
