import React, {Component} from 'react';
import UnifiedCloud from './components/UnifiedCloud';
import './App.css';
import heatingDevice1 from './data/devices/heating-device-1.json';
import heatingDevice2 from './data/devices/heating-device-2.json';
import heatingDevice3 from './data/devices/heating-device-3.json';
import firmware from './data/services/firmware.json';
import heating from './data/services/heating.json';
import IntentCreator from './components/IntentCreator';
import {Layout, Row, Col, Card} from 'antd';
import Intents from './components/Intents';

const {Content, Header} = Layout;

const defaultIntents = [
  {
    name: 'Leaving Home',
    trigger: 'ACTION',
    featureList: {
      targetIndoorTemperature: {value: 17},
      fanSpeed: {value: 'auto'},
      horizontalAirflowDirection: {value: 'auto'},
    },
    serviceName: 'heating',
  },
  {
    name: 'Arriving Home',
    trigger: 'ACTION',
    featureList: {
      targetIndoorTemperature: {
        value: 22,
      },
      fanSpeed: {
        value: 'manual',
      },
      fanSpeedLevel: {
        value: '3',
      },
      serviceName: 'heating',
    },
  },
];

class App extends Component {
  state = {
    devices: [heatingDevice1, heatingDevice2, heatingDevice3],
    services: [heating, firmware],
    intents: defaultIntents.reduce(
      (prevValue, intent) => ({...prevValue, [intent.name]: intent}),
      {}
    ),
  };

  applyIntentLikeABackend = name => {
    const intentFeaturesEntries = Object.entries(
      this.state.intents[name].featureList
    );

    const applyIntentFeaturesToDevice = device => {
      const updatedCapabilities = intentFeaturesEntries.reduce(
        (capabilities, [capability, {value}]) => {
          if (capabilities[capability]) {
            return {
              ...capabilities,
              [capability]: {...capabilities[capability], value},
            };
          }
          return capabilities;
        },
        device.capabilities
      );

      return {
        ...device,
        capabilities: updatedCapabilities,
      };
    };
    this.setState({
      devices: this.state.devices.map(applyIntentFeaturesToDevice),
    });
  };

  render() {
    return (
      <Layout>
        <Header><h1 style={{color: "white"}}>INTENT within the Unified Cloud Platform</h1></Header>
        <Content style={{padding: '0 50px'}}>
          <div style={{padding: 24, minHeight: 280}}>
            <h5>
              This part simulates a certain front-end application: The App, DCS,
              a 3rd party integration
            </h5>
            <Row className="paragraph">
              <h2>Current Automatic Processes</h2>
              <Row type="flex" justify="space-around">
                <Intents
                  intents={Object.values(this.state.intents)}
                  onApplyIntent={this.applyIntentLikeABackend}
                />
              </Row>
            </Row>
            <Row className="paragraph">
              <IntentCreator
                services={this.state.services}
                onSubmitIntent={intent =>
                  this.setState({
                    intents: {...this.state.intents, [intent.name]: intent},
                  })
                }
              />
            </Row>
            <Row className="paragraph">
              <h3>Devices</h3>
              {this.state.devices.map(device => {
                return (
                  <Col
                    span="8"
                    style={{
                      borderColor: 'black',
                      borderWidth: 1,
                      borderLeft: 'black',
                      paddingLeft: '10px',
                    }}
                  >
                    <div style={{background: '#ECECEC', padding: '30px'}}>
                      <Card title={device.name} bordered={false}>
                        {Object.entries(device.capabilities).map(
                          ([key, {value}]) => (
                            <Row type="flex" justify="space-between">
                              <Col>{key}</Col>
                              <Col>{value}</Col>
                            </Row>
                          )
                        )}
                      </Card>
                    </div>
                  </Col>
                );
              })}
            </Row>
            <hr style={{marginTop: '100px', marginBottom: '100px'}} />
            <h5>
              This part simulates what happens in the Unified Cloud Platform
            </h5>
            <Row>
              <UnifiedCloud
                intents={Object.values(this.state.intents)}
                devices={this.state.devices}
                services={this.state.services}
              />
            </Row>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default App;
